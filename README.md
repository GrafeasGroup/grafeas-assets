# Grafeas Assets

This repository acts as a backup of what is currently available at [assets.grafeas.org](https://assets.grafeas.org/). Structuring this as a git repository has known limitations, not the least of which is that [it is bad to track binary files in git](https://stackoverflow.com/a/4697279). In the meantime we intend to use this as a routing layer and central location to store non-trivial media content (e.g., audio, video, or non-web documents) apart from the main site, [www.grafeas.org](https://www.grafeas.org/).

Given we expect to outgrow this format for deploying media assets, this document will be updated with the most current method for distributing CDN-style assets.

## Adding new assets

Anything added to the `public/` directory will be available under `https://assets.grafeas.org/`, such as `public/my/file.mp4` will be accessible at `https://assets.grafeas.org/my/file.mp4`.
